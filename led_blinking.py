from machine import Pin
import time
class LED():
    def led_blink(self):
        P16 = Pin(16,Pin.OUT)
        for i in range(1):
            P16.on()
            time.sleep_ms(1000)
            P16.off()
            time.sleep_ms(1000)