import network
class ap_setup():
    def setup_ap(self):
        ssid = "SAPCON_ESP8266"
        password = "SAPCON123"
        ap = network.WLAN(network.AP_IF)
        ap.active(True)
        ap.config(essid=ssid, password=password)
        while ap.active() == False:
            pass
        print('Connection successful')
        print(ap.ifconfig())
