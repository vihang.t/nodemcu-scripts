class connect_to():
    def do_connect(self,ssid,passw):
        import network
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
        if not wlan.isconnected():
            print('connecting to network...')
            wlan.connect(ssid,passw)
            while not wlan.isconnected():
                pass
        print('network config:', wlan.ifconfig())