from led_blinking import LED
from myuart_tx import MYUART
from network_conn import connect_to
from machine import Pin
from web_page_class import html_web_page
from Access_point_setup import ap_setup
try:
    import usocket as socket
except:
    import socket
led_at_gpio16 = LED()
mytransmit_obj = MYUART()
net_obj = connect_to()
#net_obj.do_connect("Pixel_3A_XL","123456789")
ap_obj = ap_setup()
ap_obj.setup_ap()
web_page_obj = html_web_page()
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

led = Pin(4, Pin.OUT)
while True:
    #led_at_gpio16.led_blink()
    #mytransmit_obj.mytransmit()
    print("Before s.accept()")
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    led_on = request.find('/?led=on')
    led_off = request.find('/?led=off')
    if led_on == 6:
        print('LED ON')
        led.value(1)
    if led_off == 6:
        print('LED OFF')
        led.value(0)
    response = web_page_obj.web_page(led)
    conn.send('HTTP/1.1 200 OK\n')
    conn.send('Content-Type: text/html\n')
    conn.send('Connection: close\n\n')
    conn.sendall(response)
    conn.close()
