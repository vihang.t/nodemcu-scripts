from machine import Pin, Timer, SoftI2C
from time import sleep_ms
import ubluetooth
from machine import UART
import time
from machine import RTC
import os
from file_handling import file_rw
class BLE():
    def __init__(self, name):   
        self.name = name
        self.ble = ubluetooth.BLE()
        self.ble.active(True)
        self.led = Pin(15, Pin.OUT)
        self.timer1 = Timer(0)
        self.timer2 = Timer(1)
        self.disconnected()
        self.ble.irq(self.ble_irq)
        self.register()
        self.advertiser()
        self.uart = UART(2, baudrate=9600)
        #self.uart.irq(handler = self.send_to_ble)
        self.rtc = RTC()

    def connected(self):        
        self.timer1.deinit()
        self.timer2.deinit()

    def disconnected(self):        
        self.timer1.init(period=1000, mode=Timer.PERIODIC, callback=lambda t: self.led(1))
        sleep_ms(200)
        self.timer2.init(period=1000, mode=Timer.PERIODIC, callback=lambda t: self.led(0))   

    def ble_irq(self, event, data):
        if event == 1:
            '''Central disconnected'''
            self.connected()
            self.led(1)
        
        elif event == 2:
            '''Central disconnected'''
            self.advertiser()
            self.disconnected()
        
        elif event == 3:
            '''New message received'''            
            buffer = self.ble.gatts_read(self.rx)
            message = buffer.decode('UTF-8').strip()
            print(message)
            self.mytransmit(message)
            if message == 'Hi':
                #print(data)
                self.send(message)
            elif message[0:3] == "SDT":
                self.rtc.datetime((int(message[4:8]),int(message[9:11]),int(message[12:14]),int(message[14]),int(message[15:17]),int(message[18:]),0,0))
                
                                
    def register(self):        
        # Nordic UART Service (NUS)
        NUS_UUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
        RX_UUID = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E'
        TX_UUID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E'
            
        BLE_NUS = ubluetooth.UUID(NUS_UUID)
        BLE_RX = (ubluetooth.UUID(RX_UUID), ubluetooth.FLAG_WRITE)
        BLE_TX = (ubluetooth.UUID(TX_UUID), ubluetooth.FLAG_NOTIFY)
            
        BLE_UART = (BLE_NUS, (BLE_TX, BLE_RX,))
        SERVICES = (BLE_UART, )
        ((self.tx, self.rx,), ) = self.ble.gatts_register_services(SERVICES)

    def send(self, data):
        if data is not None:
            self.ble.gatts_notify(0, self.tx, data + '\n')

    def advertiser(self):
        name = bytes(self.name, 'UTF-8')
        self.ble.gap_advertise(100, bytearray('\x02\x01\x02','UTF-8') + bytearray((len(name) + 1, 0x09)) + name)
        
    def mytransmit(self,message):
        uart = UART(2, baudrate=9600)
        uart.write(message)
        sleep_ms(1000)
        self.send_to_ble()
        
    def send_to_ble(self):
        message = self.uart.read()
        self.send(message)
        f = file_rw()
        #f.write_to_file("file1.txt",str(message))
        data = f.read_file("file1.txt")
        #print(data)
        