from machine import RTC

class DATETIME_STR():
    def get_datetime_str1(self):
        rtc = RTC()
        datetime_list = list(rtc.datetime())
        #print(datetime_list)
        #YYYY-MM-DD HH:MM:SS format
        datetime_list[4] += 5
        datetime_list[5] += 30
        if datetime_list[5] > 59:
            datetime_list[4] += 1
            datetime_list[5] -= 60
        if datetime_list[4] > 23:
            datetime_list[2] += 1
            datetime_list[3] += 1
        cur_date = str(datetime_list[0]) + '-' + str(datetime_list[1]) + '-' + str(datetime_list[2])
        cur_time = str(datetime_list[4]) + ':' + str(datetime_list[5]) + ':' + str(datetime_list[6])
        return cur_date,cur_time
