from machine import Pin
import time
class LED():
    def led_blink(self):
        P15 = Pin(15,Pin.OUT)
        for i in range(1):
            P15.on()
            time.sleep_ms(2000)
            P15.off()
            time.sleep_ms(2000)
