from micropython import const
from led_blinking import LED
from get_datetime_RTC import DATETIME_STR
from machine import Pin,SoftI2C,Timer
import SSD1306
import time
import ntptime
from network_conn import connect_to
import os
from ble_sample import BLE
import asyncio
from Access_point_setup import ap_setup
from file_handling import file_rw
    
i2c = SoftI2C(scl= Pin(22), sda = Pin(21))
oled_width = 128
oled_height = 64
oled = SSD1306.SSD1306_I2C(oled_width,oled_height,i2c)

led_at_gpio15 = LED()
net_obj = connect_to()
net_obj.do_connect("ssid","password123")

cur_rtc = DATETIME_STR()
ntptime.settime()
[cur_date,cur_time,datetime_list] = cur_rtc.get_datetime_str1()
oled.text("Date {}".format(cur_date),0,0)
oled.text("Time {}".format(cur_time),0,30)
print(datetime_list[4:7])
print(True if datetime_list[4] == 10 else False)
print(cur_date)
print(cur_time)
oled.show()

AL_led = Pin(12, Pin.OUT)
ble = BLE("sapcon ESP32")
#uart=UART(2, baudrate=9600)
#uart_reader = asyncio.StreamReader(uart)
print(os.listdir())
print(os.uname())
f_read = file_rw()
data1 = f_read.read_file("file1.txt")
data2 = data1.split("\n")
data = []
for val in data2:
    if len(val)>0:
        data.append(val)
print(data)
#for debug Purpose
'''
print(data[4:12])
print(int(data[4:6]))
print(int(data[7:9]))
print(int(data[10:12]))
'''
timer3 = Timer(2)
timer4 = Timer(3)
while True:
    time.sleep_ms(1000)
    oled.fill(0)
    time.sleep_ms(50)              
    [cur_date,cur_time,datetime_list] = cur_rtc.get_datetime_str1()
    #debug_purpose
    #ble.mytransmit(str(datetime_list[4:7]))
    for data3 in data:
        if int(data3[4:6])==datetime_list[4]:
            #debug purpose
            #ble.mytransmit('/' + data[4:6] + '/')
            if int(data3[7:9])==datetime_list[5]:
                #for debug purpose
                #ble.mytransmit('/' + data[7:9] + '/')
                #ble.mytransmit('<'+data[10:12] + '>')
                #ble.mytransmit('<'+str(datetime_list[6]) + '>')
                if int(data3[10:12]) <= datetime_list[6] and int(data3[12:14]) > datetime_list[6]:
                    ble.mytransmit("Alarm ON")
                    timer3.init(period=1000, mode=Timer.ONE_SHOT, callback=lambda t: AL_led(1))
                    time.sleep_ms(200)
                    timer4.init(period=30000, mode=Timer.ONE_SHOT, callback=lambda t: AL_led(0))
    oled.text("Date {}".format(cur_date),0,0)
    oled.text("Time {}".format(cur_time),0,30)
    oled.show()
    #message = await uart_reader.read(-1) 
    ble.send_to_ble()
    
