
class file_rw():
    def write_to_file(self,fname,data):
        f = open(fname,'a')
        f.write(data)
        f.close()
    
    def read_file(self,fname):
        f = open(fname)
        data = f.read()
        f.close()
        return data